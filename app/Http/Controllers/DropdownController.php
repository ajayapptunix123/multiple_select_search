<?php

namespace App\Http\Controllers;
use App\Models\Dropdown;
Use App\Models\Country;
use Illuminate\Http\Request;

class DropdownController extends Controller
{
    public function index(Request $request)
    {


        $list=Country::all();
        return view('home', compact('list'));
}
// public function selectSearch(Request $request)
// {

//     $countries = [];

//     if($request->has('q')){
//         $search = $request->q;
//         $countries =Country::select("id", "name")
//                 ->where('name', 'LIKE', "%$search%")
//                 ->get();
//     }

//     return response()->json($countries);
// }
    public function postData(Request $request)
{
    $input = $request->except('_token');

    $input['cat'] = json_encode($input['cat']);
    $input['country'] = json_encode($input['country']);
    // $input['cat'] = json_encode($input['cat']);




    Dropdown::create($input);

    return back()->with('success','Post created successfully.');
}


}
