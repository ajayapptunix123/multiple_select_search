<?php

namespace App\Http\Controllers;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index(Request $request)
    {


        // $user=Country::all();
        //    echo'<pre>';
        //    print_r($user);
        //    die();

            return view('welcome');
    }

    public function selectSearch(Request $request)
    {

    	$countries = [];

        if($request->has('q')){
            $search = $request->q;
            $countries =Country::select("id", "name")
            		->where('name', 'LIKE', "%$search%")
            		->get();
        }

        return response()->json($countries);
}
}
