<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\DropdownController;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [CountryController::class, 'index']);
Route::get('/ajax-autocomplete-search', [CountryController::class, 'selectSearch']);
// Route::get('/post', function () {
//     return view('home');
// });
Route::get('/post', [DropdownController::class, 'index'])->name('post');
Route::get('/ajax-autocomplete-search', [DropdownController::class, 'selectSearch']);

Route::post('/postData', [DropdownController::class, 'postData'])->name('postData');


